# Docker Compose LEMP Stack

LEMP means Linux based Nginx + MariaDB + PHP-PFM stack.

## Details

The following versions are used.

* PHP 7 FPM - based on oficial Alpine version, include the most common extensions (PDO, MySQL, GD, ImageMagick, Multibyte String, Exif)
* Nginx on Alpine
* MariaDB 10

## Configuration

The folders's naming reflects the structure inside containers - the folder structure in the real environment.

## Pre-requisities

GIT
Docker
Docker compose

## How to run the stack 

### Clone this repository.

SSH: git@gitlab.com:pducho/lemp.git
HTTPS: https://gitlab.com/pducho/lemp.git

### Start the server.

Start the stack with docker compose: `docker-compose up -d`.

#### Configuration

The stack should start up with the configs added.
The setup is my initial version for running Wordpress websites
you might need to improve configurations.

#### Entering the containers

You can use the following command to enter a container:

Where `{CONTAINER_NAME}` is one of:

`docker exec -tiu root {CONTAINER_NAME} sh`

* myapp-php
* myapp-nginx
* myapp-mariadb

When you want to add bash in Alpine container run `apk add bash`.

#### Entering the database

For database management you can use adminer https://www.adminer.org placed in workdir.
TODO: PHPmyadmin container included but it needs to be exposed or 

## TODO

Add certbot for SSL certs solution.
